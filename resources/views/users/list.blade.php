<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Users
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn green" id="add_new_course" data-action="{{route('admin.users.init')}}">
                            Add New <i class="fa fa-plus"></i>
                        </button>                        
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Name
                    </th>
                    <th valign="middle">
                        Email
                    </th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr class="odd gradeX" id="data-row-{{$user->id}}">
                    <td valign="middle">
                        {{$user->name}}
                    </td>
                    <td valign="middle">{{$user->email}}
                        
                    </td>
                    <td valign="middle">
                        {{$user->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.users.edit',['id'=>$user->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        @if($user->decommissioned ==0)<a href="{{route('admin.users.delete',['id'=>$user->id])}}" class="btn red remove-course" ><i class="fa fa-remove"></i> Decommission</a>@endif 
                        @if($user->decommissioned ==1)<a href="{{route('admin.users.recommission',['id'=>$user->id])}}" class="btn grey recommission_user" ><i class="fa fa-play"></i> Recommission</a>@endif 
                        <a href="{{route('admin.tasks.calendar',['user_id'=>$user->id])}}" class="btn blue" target="_blank" ><i class="fa fa-calendar"></i> Calendar</a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>