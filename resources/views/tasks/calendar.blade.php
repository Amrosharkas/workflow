<?php
$i = 'tasks';
$j = 'calendar';
?>
@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/dragula/dragula.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/codebase/dhtmlxscheduler.css')}}" type="text/css" media="screen" title="no title" charset="utf-8">
<style type="text/css" media="screen">

	html, body{
		margin:0;
		padding:0;
		
		
	}

	.dhx_cal_event div.dhx_footer,
	.dhx_cal_event.past_event div.dhx_footer,
	.dhx_cal_event.event_color1 div.dhx_footer,
	.dhx_cal_event.event_color2 div.dhx_footer,
	.dhx_cal_event.event_color3 div.dhx_footer{
		background-color: transparent !important;
	}
	.dhx_cal_event .dhx_body{
		-webkit-transition: opacity 0.1s;
		transition: opacity 0.1s;
		opacity: 0.7;
	}
	.dhx_cal_event .dhx_title{
		line-height: 12px;
	}
	.dhx_cal_event_line:hover,
	.dhx_cal_event:hover .dhx_body,
	.dhx_cal_event.selected .dhx_body,
	.dhx_cal_event.dhx_cal_select_menu .dhx_body{
		opacity: 1;
	}
	<?php foreach($colors as $color){?>
	.dhx_cal_event.event_color<?php echo $color->id;?> div, .dhx_cal_event_line.event_color<?php echo $color->id;?>{
		background-color: <?php echo $color->color_code;?> !important;
		border-color: <?php echo $color->color_code;?> !important;
	}
	.dhx_cal_event_clear.event_color2{
		color:orange !important;
	}
	<?php ;}?>
#scheduler_here,#pjax-container{
	height:800px ! !important;
}
</style>

@endSection

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jsvalidation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/dragula/dragula.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.js')}}"></script>
@endSection

@section('page_js')
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
<script src="{{asset('assets/codebase/dhtmlxscheduler.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('assets/codebase/ext/dhtmlxscheduler_quick_info.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
function init() {
    // Coloring --------------------------------------------------------
    scheduler.templates.event_class = function (start, end, event) {
        var css = "";

        if (event.subject) // if event has subject property then special class should be assigned
            css += "event_" + event.subject;

        if (event.id == scheduler.getState().select_id) {
            css += " selected";
        }
        return css; // default return

        /*
        	Note that it is possible to create more complex checks
        	events with the same properties could have different CSS classes depending on the current view:

        	var mode = scheduler.getState().mode;
        	if(mode == "day"){
        		// custom logic here
        	}
        	else {
        		// custom logic here
        	}
        */
    };

    var subject = [
        <?php foreach($colors as $color){?>
        {
            key: 'color<?php echo $color->id;?>',
            label: 'Color<?php echo $color->id;?>'
        },
		<?php ;}?>
        
			];
    // starting hour  ---------------------------------------------------
    //scheduler.config.first_hour = 6;
    // Touch support ---------------------------------------------------
    scheduler.config.touch = "force";
    // Y- Axis Format  ----------------------------------------------------
    scheduler.config.start_on_monday = false;
    scheduler.config.xml_date = "%Y-%m-%d %H:%i";
    scheduler.config.hour_date = "%h:%i %A";
    //scheduler.config.hour_size_px=25;
    scheduler.config.xml_date = "%Y-%m-%d %H:%i";
    scheduler.init('scheduler_here', new Date(<?php echo date("Y");?>, <?php echo date("m");?>-1, <?php echo date("d");?>), "week");
    scheduler.parse([
		<?php foreach($logs as $log){?>
        {
            start_date: "<?php echo date("Y-m-d H:i",strtotime($log->start_time));?>",
            end_date: "<?php echo date("Y-m-d H:i",strtotime($log->end_time));?>",
            text: "<?php echo $log->name;?>",
            subject: 'color<?php echo $log->color_id;?>'
        },
		<?php ;}?>
        
			], "json");

}
 init();
</script>


@endSection

@section('add_inits')

@stop

@section('title')
Calendar
@stop

@section('page_title')
Calendar
@stop

@section('page_title_small')

@stop

@section('content')
	<div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:800px;'>
		<div class="dhx_cal_navline">
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
			<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
			<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
		</div>
		<div class="dhx_cal_header">
		</div>
		<div class="dhx_cal_data">
		</div>
	</div>

@stop

