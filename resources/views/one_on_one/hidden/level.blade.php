<div class="portlet box blue-hoki level" data-id="{{$level?$level->id:''}}">
    <div class="portlet-title">
        <div class="caption">
            {{$level ? $level->title: ''}} </div>
        <div class="actions tools">
            <button class="btn btn-default btn-sm remove_level"
                    data-action="{{$level?route('admin.one_on_one.delete_level',['id'=>$level->id]):''}}"
                    type="button"><i class="fa fa-remove"></i> Remove</button>
            <a href="javascript:;" class="expand"> </a>
        </div>
    </div>
    <div class="portlet-body display-hide">
        <input type="hidden" value="{{$level ? $level->id: ''}}" name="id">
        <div class="form-group">
            <label>Title</label>
            <input class="form-control" name="title" type="text" value="{{$level?$level->title:''}}">
        </div>
        <!--- IG -->
        <div class="form-group ig">
            <label >IG</label>
            <input type="file" class="hidden fileinput"
                   data-action="{{$level?route('admin.one_on_one.upload_ig',['id'=>$level->id]):''}}" 
                   data-delete="{{$level?route('admin.one_on_one.delete_ig',['id'=>$level->id]):''}}"
                   >
            <div class="row">
                <div class="col-md-8">
                    <span class="filenameplaceholder">
                        @if($level && $level->ig())
                        <a href="{{route('admin.one_on_one.ig',['id'=>$level->id])}}" class="filename">
                            {{$level->ig()->name}}
                        </a>
                        @else
                        No file selected
                        @endif
                    </span>
                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                        <span>0%</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-primary filepicker pull-right {{$level && $level->ig()?'hidden':''}}">
                        Select File
                    </button>
                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                    <button class="btn btn-danger pull-right removefile {{$level && $level->ig()?'':'hidden'}}" 
                            data-action="{{$level?route('admin.one_on_one.delete_ig',['id'=>$level->id]):''}}"
                            >
                        Remove
                    </button>
                </div>
            </div>
        </div>
        <!-- end IG !-->
        <!-- Material -->
        <div class="form-group material">
            <label >Material</label>
            <input type="file" class="hidden fileinput"
                   data-action="{{$level?route('admin.one_on_one.upload_material',['id'=>$level->id]):''}}" 
                   data-delete="{{$level?route('admin.one_on_one.delete_material',['id'=>$level->id]):''}}"
                   >
            <div class="row">
                <div class="col-md-8">
                    <span class="filenameplaceholder">
                        @if($level && $level->material())
                        <a href="{{route('admin.one_on_one.ig',['id'=>$level->id])}}" class="filename">
                            {{$level->material()->name}}
                        </a>
                        @else
                        No file selected
                        @endif
                    </span>
                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                        <span>0%</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-primary filepicker pull-right {{$level && $level->material()?'hidden':''}}">
                        Select File
                    </button>
                    <button class="btn btn-danger hidden pull-right cancel">Cancel</button>
                    <button class="btn btn-danger pull-right removefile {{$level && $level->material()?'':'hidden'}}" 
                            data-action="{{$level?route('admin.one_on_one.delete_material',['id'=>$level->id]):''}}"
                            >
                        Remove
                    </button>
                </div>
            </div>
        </div>
        <!-- end Material -->


    </div>
</div>
