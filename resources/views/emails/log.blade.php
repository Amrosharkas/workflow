<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="border:2px solid #92278F; padding:15px; margin:7px;">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle"><img src="http://www.dtpfactory.com/imgs/dtp-logo.png" style="max-width:100%;" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top" bgcolor="#92278F"><div></div></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">
    <h3>{{$task->name}} ||  {{$user->name}}</h3>
    <table border="1" cellpadding="10" class="table table-striped table-bordered table-hover table-dt"  >
                <thead>
                    <tr class="tr-head">
                        <th valign="middle">
                            Start time
                        </th>
                        <th valign="middle">
                            End time
                        </th>
                        <th valign="middle">User</th>
                        <th valign="middle">By</th>
                        <th valign="middle">
                            Created at
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($logs as $log)
                    <tr class="odd gradeX" id="data-row-{{$log->id}}">
                        <td valign="middle">
                            {{ Carbon\Carbon::parse($log->start_time)->format('D d, M Y H:i a') }}
                        </td>
                        <td valign="middle">
                        @if($log->end_time){{ Carbon\Carbon::parse($log->end_time)->format('D d, M Y H:i a') }}@endif
                        @if(!$log->end_time) Null @endif    
                        </td>
                        <td valign="middle">{{$log->getUser->name}}</td>
                        <td valign="middle">{{$log->getAddedBy->name}}</td>
                        <td valign="middle">
                            
                            {{ Carbon\Carbon::parse($log->created_at)->format('D d, M Y H:i a') }}
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            
            </td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top" bgcolor="#92278F"><div></div></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">DTP Factory © 2016</td>
  </tr>
</table>
</body>
</html>