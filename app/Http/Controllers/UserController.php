<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Hash;

class UserController extends Controller
{
    public function index($status){
    	$users = User::where('user_type','User')->where('decommissioned',$status)->get();
        $data = [];
        $data['partialView'] = 'users.list';
        $data['status'] = $status;
        $data['users'] = $users;
    	return view('users.index',$data);
    }

    public function init(){
    	
    	return response()->json([
                    'url' => route('admin.users.edit', ['id' => 0])
        ]);
    }
    
    

    
    public function edit($id)
    {
        $user = User::find($id);
        $data = [];
        $data['partialView'] = 'users.form';
        $data['user'] = $user;
        

        return view('tasks.index',$data);

    }


    public function store(Request $request)
    {
        $data = $request->input();
        $user = User::find($data['id']);
        if(!$user){
        	$user = new User();
        }
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->user_type = 'User';
        $user->save();
    }

    public function delete($id){
        $user = User::find($id);
        $user->decommissioned = 1;
        $user->save();
        return 'Success';
    }

    public function recommission($id){
        $user = User::find($id);
        $user->decommissioned = 0;
        $user->save();
        return 'Success';    
    }
}
