<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Task;
use App\task_progress;
use App\User;
use App\Language;
use Auth;
use DB;
use App\Color;
use Mail;

class TaskController extends Controller
{
    protected $logged_user;
    public function __construct(){
        $logged_user  = Auth::user();
        $this->logged_user = $logged_user;
        if($this->logged_user->decommissioned == 1){
            //return redirect('/logout');
            exit();
        }
    }
    public function index($status="All"){

        // if the resource allocator is logged in -> get all tasks
        if($this->logged_user->user_type == "Allocator"){
            $tasks = Task::where('approval_status',$status)->get();
            if($status == "All"){
                return redirect('/admin/tasks/getTasks/Pending');
            }
        }
        // if a normal user is logged in -> get only his tasks
        if($this->logged_user->user_type == "User"){
            if($status == "All"){
                return redirect('/admin/tasks/getTasks/Running');
            }
            if($status == "Running"){
                $status1 = array('Running','Paused');
            }else{
                $status1 = array('Finished');
            }
            $tasks =Task::where('user_id',$this->logged_user->id)->whereIn('status',$status1)->get();
            $updateNotification =Task::where('user_id',$this->logged_user->id)->whereIn('status',$status1)->update(['notification'=>0]);

        }
        $data = [];
        $data['status'] = $status;
        $data['partialView'] = 'tasks.list';
        $data['tasks'] = $tasks;
        $data['user_type'] = $this->logged_user->user_type;
    	return view('tasks.index',$data);
    }

    public function init(){
    	
    	return response()->json([
                    'url' => route('admin.tasks.edit', ['id' => 0])
        ]);
    }
    
    

    
    public function edit($id)
    {
        $task = Task::find($id);
        $data = [];
        $data['partialView'] = 'tasks.form';
        $data['task'] = $task;
        $data['languages'] = Language::orderBy('language')->get();
        return view('tasks.index',$data);

    }

    public function details($id){
        $task = Task::find($id);
        $data = [];
        $data['partialView'] = 'tasks.task_details';
        $data['task'] = $task;
        return view('tasks.logs',$data);
        
    }

    public function store(Request $request)
    {
        $data = $request->input();
        $task_check = Task::find($data['id']);
        if(!$task_check){
            $task = new Task();
            $last_task = Task::where('user_id',$this->logged_user->id)->orderBy('id','desc')->first();
            if(isset($last_task) && $last_task->color_id != 30){
                $new_color_id = $last_task->color_id + 1;    
            }else{
                $new_color_id = 1;
            }
            $task->color_id = $new_color_id;
        }else{
            $task = $task_check;
        }
        // Validate task start time
        $response = $this->validateTask(strtotime($data['start_time']),"0",$this->logged_user->id);
        
        if($response['type'] == "error"){
            return response()->json([
                'data' => $response
            ]);    
        }
        // Set the task attributes
        $task->name = $data['name'];
        $task->user_id = $this->logged_user->id;
        $task->start_time = $data['start_time'];
        $task->last_start_time = $data['start_time'];
        $task->software = $data['software'];
        $task->estimated_time_hours = $data['estimated_time_hours'];
        $task->estimated_time_mins = $data['estimated_time_mins'];
        $task->project_type = $data['project_type'];
        $task->language_source = $data['source'];
        $task->language_target = $data['target'];
        $task->email_subject = $data['email_subject'];
        $task->share_path = $data['share_path'];
        $task->save();

        if(!$task_check){
            $log = new task_progress();
            $log->task_id = $task->id;
            $log->start_time = $data['start_time'];
            $log->user_id = $this->logged_user->id;
            $log->added_by_id = $this->logged_user->id;
            $log->save();
        }
        // Ready the response for the success
        $response = [];
        $response['type'] = 'success';
        $response['msg'] = 'Successfully added';
        $response['task_id'] = $log->task_id;
        return response()->json([
            'data' => $response
        ]);

    }

    public function delete($id){
        $task = Task::find($id)->delete();
        return 'Success';
    }

    public function logs($id)
    {
        $task = Task::find($id);
        $logs = task_progress::where('task_id',$id)->orderBy('start_time','asc')->get();
        $data = [];
        $data['partialView'] = 'tasks.logs_detail';
        $data['task'] = $task;
        $data['logs'] = $logs;
        $data['users'] = User::where('user_type','User')->orderBy('name')->get();

        return view('tasks.logs',$data);

    }

    public function addLog(Request $request){
        $data = $request->input(); 
        $start_time = strtotime($data['start_time']);
        $end_time = strtotime($data['end_time']);
        //Validate the task
        $response = $this->validateTask($start_time,$end_time,$data['user_id']);
        if($response['type'] == "error"){
            return response()->json([
                'data' => $response
            ]);    
        }
        // create log and save it
        $log = new task_progress();
        $log->start_time = $data['start_time'];
        $log->end_time = $data['end_time'];
        $log->task_id = $data['task_id'];
        $log->user_id = $data['user_id'];
        $log->added_by_id = $this->logged_user->id;
        $log->save();

        //Update task total duration
        $log_duration = strtotime($log->end_time) - strtotime($log->start_time);
        $task = Task::find($log->task_id);
        $task->duration = $task->duration + $log_duration;
        $task->save();
       $this->updateStartedOn($task->id);

        // Ready the response for the success
        $response = [];
        $response['type'] = 'success';
        $response['msg'] = 'Successfully added';
        $response['task_id'] = $log->task_id;
        return response()->json([
            'data' => $response
        ]);
    }

    public function validateTask($start_time,$end_time,$user_id){
        $response = [];
        $response['type'] = 'success';
        $response['msg'] = 'Successfully added';
        
        $start_time = date("Y-m-d H:i:s",$start_time);
        if($end_time!= "0"){
            $end_time = date("Y-m-d H:i:s",$end_time);
        }
        // Get logs interfering with start time
        $check_interfere_start = task_progress::where('start_time' ,'<=',$start_time)->where('end_time' ,'>',$start_time)->where('user_id',$user_id)->count();
        
        // Get logs interfering with end time
        $check_interfere_end = task_progress::where('start_time' ,'<',$end_time)->where('end_time' ,'>=',$end_time)->where('user_id',$user_id)->count();
        // if the insertion is from adding new task pass this check
        if($end_time == "0"){
            $check_interfere_end = 0;    
        }

        //Check if start time greater than end time
        if($start_time>=$end_time && $end_time != "0"){
            
            $response['type'] = 'error';
            $response['msg'] = 'Start time is bigger than end time';
            
           
             
        }

        //Check if the interfering tasks exist
        if($check_interfere_start > 0 || $check_interfere_end > 0){
            
            $response['type'] = 'error';
            $response['msg'] = 'The interval inserted is interfering with an old log';
            
            
             
        }
        return $response;


    }
    public function deleteLog($id){
        $log = task_progress::find($id);
        if($log->end_time){
            $log_duration = strtotime($log->end_time) - strtotime($log->start_time);
            $task = Task::find($log->task_id);
            $task->duration = $task->duration - $log_duration;
            
        }
        $remaining_logs = task_progress::where('task_id',$log->task_id)->count();
        if($remaining_logs>=2){
            $task->save();
            $log->delete();
           $this->updateStartedOn($task->id);
        }else{
            abort(404);
        }
        return 'Success';
    }

    public function updateStatus($id,$status){
        $task = Task::find($id);
        $task->status = $status;
        if($status == "Finished"){
            $task->approval_status = "Pending";
        }else{
            $task->approval_status = "None"; 
        }
        $task->save();
    }
    public function updateApprovalStatus($id,$status,$comment){
        $task = Task::find($id);
        $user = User::find($task->user_id);
        $task->approval_status = $status;
        $task->comment = $comment;
        if($task->approval_status != "Pending"){
            $task->notification = 1;
            Mail::send('emails.approval', ['user' => $user,'task' => $task  ], function ($m) use ($user,$task) {
                $m->from('workflow@arabiclocalizer.info', 'DTP Factory');

                $m->to('mtarek@arabiclocalizer.com', 'Resource Allocator')->subject($task->name.' Approved');
                
            });
        }
        $task->save();
        $remaining_pending = Task::where('approval_status','Pending')->count();
        return $remaining_pending;
    }
    public function updateTaskStatus(Request $Request){
        $data = $Request->input();
        $response = [];
        $response['update_running'] = 0;
        $task = Task::find($data['task_id']);
        $user = User::find($task->user_id);
        $logs = task_progress::where('task_id',$task->id);
        $log = task_progress::whereNull('end_time')->where('task_id',$data['task_id'])->first();
        if($log){
            $response = $this->validateTask(strtotime($log->start_time),strtotime($data['time']),$task->user_id);
            if($response['type'] == "error"){
                return response()->json([
                    'data' => $response
                ]);    
            }
            $log->end_time = $data['time'];
            $log_duration = strtotime($log->end_time) - strtotime($log->start_time);
            if($this->checkDay($task->id,$data['time']) == 1){
                $dateDay = date("Y-m-d",strtotime($log->start_time));
                $end_time1 = $dateDay." 23:59:59";
                $start_time2 = strtotime($end_time1) + 2;
                $start_time2 = date("Y-m-d",$start_time2);
                $log->end_time = $end_time1;
                $log2 = new task_progress();
                $log2->task_id = $data['task_id'];
                $log2->user_id = $this->logged_user->id;
                $log2->added_by_id = $this->logged_user->id;
                $log2->start_time = $start_time2;
                $log2->end_time = $data['time'];
                $log2->save();
            }
            $log->save();
            if($data['log_type'] == "pause"){
                $task->status = "Paused";
                $task->duration = $task->duration + $log_duration;
                

            }
            if($data['log_type'] == "end"){
                $task->status = "Finished";
                $task->approval_status = "Pending"; 
                $task->duration = $task->duration + $log_duration;
                $task->end_time = $data['time'];
                //Get remaining running tasks
                $running_tasks = Task::where('user_id',$this->logged_user->id)->whereIn('status',array('Running','Paused'))->count();
                $running_tasks = $running_tasks -1;
                $response['running'] = $running_tasks;
                $response['update_running'] = 1;

                
            }
        }else{
            $last_log = task_progress::where('task_id',$data['task_id'])->orderBy('id','DESC')->first();
            if(strtotime($data['time']) < strtotime($last_log->end_time)){
                $response['type'] = 'error';
                $response['msg'] = 'Start time is before the last paused end time';
                return response()->json([
                    'data' => $response
                ]);
            }
            $response = $this->validateTask(strtotime($data['time']),"0",$this->logged_user->id);
            if($response['type'] == "error"){
                return response()->json([
                    'data' => $response
                ]);    
            }
            $log = new task_progress();
            $log->task_id = $data['task_id'];
            $log->user_id = $this->logged_user->id;
            $log->added_by_id = $this->logged_user->id;
            $log->start_time = $data['time'];
            $log->save();
            $task->last_start_time = $data['time'];
            $task->status = "Running";
        }
       $this->updateStartedOn($task->id);
        $task->save();
        if($task->status == "Finished"){
            $user= $this->logged_user;
            $logs = task_progress::where('task_id',$data['task_id'])->orderBy('start_time','ASC')->get();
            Mail::send('emails.log', ['user' => $user,'task' => $task ,'logs' => $logs ], function ($m) use ($user,$task,$logs) {
                $m->from('workflow@arabiclocalizer.info', 'DTP Factory');

                $m->to($user->email, $user->name)->subject($task->name.' Finished');
                
            });
        }
        $response['type'] = 'success';
        $response['msg'] = 'Saved';
        return response()->json([
            'data' => $response
        ]);
    }

    public function calendar($user_id){
        if($user_id == 0){
            $user_id = $this->logged_user->id;
        }
        $colors = Color::get();
        $logs = DB::table('tasks')
            ->join('task_progress', 'tasks.id', '=', 'task_progress.task_id')
            ->select('tasks.color_id', 'tasks.name', 'task_progress.*')
            ->where('tasks.status','Finished')
            ->where('tasks.user_id',$user_id)
            ->get();
        return view('tasks.calendar',compact('colors','logs'));
    }

    public function checkDay($task_id,$end_time){
        $log = task_progress::whereNull('end_time')->where('task_id',$task_id)->first();
        if(!$log){
            return 0;
        }else{
            if(date("D",strtotime($log->start_time)) != date("D",strtotime($end_time))){
                return 1;
            }else{
                return 0;
            }
        }

    }

    public function updateStartedOn($task_id){
        $first_log = task_progress::where('task_id',$task_id)->orderBy('start_time','asc')->first();
        $task = Task::find($task_id);
        $task->start_time = $first_log->start_time;
        $task->save();
    }
    
}
