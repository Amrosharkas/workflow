<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Task;
use View;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        $user = Auth::user();
        $running = Task::whereIn('status',array('Running','Paused'))->where('user_id',$user->id)->count();
        $notification  = Task::whereIn('notification',array(1))->where('user_id',$user->id)->count();
        $pending = Task::whereIn('approval_status',array('Pending'))->count();
        View::share('running', $running);
        View::share('pending', $pending);
        View::share('notification', $notification);
        return $next($request);
    }
}
