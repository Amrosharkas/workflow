<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/admin/tasks');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin/tasks', 'as' => 'admin.tasks.'], function () {
    // Tasks
    Route::get('/', 'TaskController@index')->name('index');
    Route::get('getTasks/{status}', 'TaskController@index')->name('index');
    Route::post('init', 'TaskController@init')->name('init');
    Route::get('{id}/edit', 'TaskController@edit')->name('edit');
    Route::delete('{id}/delete', 'TaskController@delete')->name('delete');
    Route::get('create', 'TaskController@create')->name('create');
    Route::post('store', 'TaskController@store')->name('store');
    Route::patch('{id}/update', 'TaskController@update')->name('update');
    Route::get('{id}/details', 'TaskController@details')->name('details');
    // Logs
    Route::get('{id}/logs', 'TaskController@logs')->name('logs');
    Route::post('addLog', 'TaskController@addLog')->name('add_log');
    Route::delete('{id}/delete_log', 'TaskController@deleteLog')->name('delete_log');
    Route::post('{id}/{status}/updateStatus', 'TaskController@updateStatus')->name('update_status');
    Route::post('{id}/{status}/{comment}/updateApprovalStatus', 'TaskController@updateApprovalStatus')->name('update_approval_status');
    Route::post('updateTaskStatus', 'TaskController@updateTaskStatus')->name('update_task_status');
    Route::post('/checkDay', 'TaskController@checkDay')->name('check_day');
    // Calendar
    Route::get('{user_id}/calendar', 'TaskController@calendar')->name('calendar');
    
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin/users', 'as' => 'admin.users.'], function () {
    Route::get('/{status}', 'UserController@index')->name('index');

    Route::post('init', 'UserController@init')->name('init');
    Route::get('{id}/edit', 'UserController@edit')->name('edit');
    Route::delete('{id}/delete', 'UserController@delete')->name('delete');
    Route::post('{id}/recommission', 'UserController@recommission')->name('recommission');
    Route::get('create', 'UserController@create')->name('create');
    Route::delete('delete_multiple', 'UserController@deleteMultiple')->name('delete_multiple');
    Route::post('store', 'UserController@store')->name('store');
    Route::patch('{id}/update', 'UserController@update')->name('update');
});



Route::auth();

Route::get('/home', 'HomeController@index');
