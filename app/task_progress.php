<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task_progress extends Model
{
    protected $table = "task_progress";

    public function getUser(){
    	return $this->hasOne('App\User','id','user_id');
    }

    public function getAddedBy(){
    	return $this->hasOne('App\User','id','added_by_id');
    }
}
