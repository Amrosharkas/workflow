<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->integer('estimated_time_hours')->nullable();
            $table->integer('estimated_time_mins')->nullable();
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->integer('duration')->default(0);
            $table->string('software')->nullable();
            $table->string('project_type')->nullable();
            $table->string('language_source')->nullable();
            $table->string('language_target')->nullable();
            $table->string('email_subject')->nullable();
            $table->string('share_path')->nullable();
            $table->string('status')->default('Running');// Running,Paused, Finished
            $table->string('approval_status')->default('None');// None, Approved, Rejected, Pending
            $table->string('comment')->nullable();
            $table->integer('notification')->default(0);
            $table->integer('color_id')->nullable();
            $table->datetime('last_start_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
